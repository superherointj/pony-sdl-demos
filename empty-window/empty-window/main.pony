use "pony-sdl"
use "time"

actor Main
  let sdl: SDLVideo
  let timers: Timers = Timers
  let render_loop: Timer tag
  let tick_loop: Timer tag

  new create(env:Env) =>

    sdl = SDLVideo(SDLFlags.init_video(), "SDL Demo1", 640, 480)
    // sdl.load_texture("data/pony_00.png", 0)
    // sdl.load_texture("data/pony_01.png", 1)

    let rtimer = Timer(object iso is TimerNotify
                        let _game: Main = this
                        fun ref apply(timer:Timer, count:U64):Bool =>
                          _game.render()
                          true
                      end, 0, 16_666_667)
    render_loop = rtimer
    timers(consume rtimer)

    let ttimer = Timer(object iso is TimerNotify
                        let _game: Main = this
                        fun ref apply(timer:Timer, count:U64):Bool =>
                          _game.tick()
                          true
                      end, 0, 66_666_667)
    tick_loop = ttimer
    timers(consume ttimer)


  be tick() =>
    let events: Array[SDLEvent] = sdl.poll_events()
    for event in events.values() do
      match event
        | let _: SDLQuit => dispose()
        // | let down: SDLKeyDown => keydown(down)
        // | let up: SDLKeyUp => keyup(up)
      end
    end

  be render() =>
    sdl.>clear().>set_draw_color(64, 64, 64).>fill_rect()
    // for obj in _objects.values() do
    //   obj.draw(sdl)
    // end
    // for pony in _ponies.values() do
    //   pony.draw(sdl)
    // end
    sdl.present()

  be dispose() =>
    // send_bye()
    timers.cancel(render_loop)
    timers.cancel(tick_loop)
    sdl.dispose()
    // match _conn
      // | let conn: TCPConnection => conn.dispose()
    // end
