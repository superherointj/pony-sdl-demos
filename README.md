# pony-sdl-demos

Demos of SDL usage in Pony Language

[![pipeline status](https://gitlab.com/superherointj/pony-sdl-demos/badges/master/pipeline.svg)](https://gitlab.com/superherointj/pony-sdl-demos/commits/master)

Other Pony SDL examples
* [https://github.com/krig/tinyhorse](https://github.com/krig/tinyhorse)
* [https://github.com/doublec/pony-sdl-example](https://github.com/doublec/pony-sdl-example)


---

MIT Licensed